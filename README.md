#WeUI为微信Web服务量身设计

WeUI官方链接:[https://github.com/weui/weui](https://github.com/weui/weui)

###概述

这是基于WeUI框架进行扩展的框架。包括`button`、`cell`、`dialog`、`progress`、`toast`、`actionSheet`、`article`、`icon`等格式组件。增加了对`dialog`、`toast`、`actionSheet`进行封装，使用javascript API实现。

###开发

```
git clone https://git.oschina.net/ranson/weui.git
cd weui
npm install -g gulp
npm install
gulp -ws
```

###License

This plugin is licensed under the MIT license.
Copyright (c) 2016 Ranson   Power by WeUI

